using Builds;
using Nuke.Common;

sealed class Build : NukeBuild, ITest, IInspect, IClean
{
    public static int Main() => Execute<Build>(x => ((ICompile)x).Compile);
}