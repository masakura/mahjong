﻿using Nuke.Common;
using Nuke.Common.IO;
using Nuke.Common.Tools.DotNet;
using Serilog;
using static Nuke.Common.Tools.DotNet.DotNetTasks;

namespace Builds;

interface IClean : ISolution
{
    // ReSharper disable once UnusedMember.Global
    Target Clean => t => t
        .Executes(() =>
        {
            DotNetClean(s => s
                .SetProject(Solution));

            Log.Information("Delete {OutputDirectory}", OutputDirectory);
            OutputDirectory.DeleteDirectory();
        });
}