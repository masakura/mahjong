﻿using System.IO;
using Newtonsoft.Json;
using Nuke.Common;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.ReSharper;
using static Nuke.Common.Tools.ReSharper.ReSharperTasks;

namespace Builds;

interface IInspect : ICompile
{
    // ReSharper disable once UnusedMember.Global
    Target Inspect => t => t
        .DependsOn(Compile)
        .Executes(() =>
        {
            var report = ReportsDirectory / "inspections" / "inspections.sarif";

            ReSharperInspectCode(s => s
                .SetTargetPath(Solution)
                .SetProperty("Configuration", Configuration)
                .SetOutput(report)
                .SetFormat("Sarif")
                .SetCachesHome(CacheDirectory / "inspections")
                .SetSeverity(ReSharperSeverity.HINT)
                .SetProcessArgumentConfigurator(a => a
                    .Add("--no-build")));

            var sarif = JsonConvert.DeserializeObject<dynamic>(File.ReadAllText(report));
            if (sarif.runs[0].results.ToString() != "[]") Assert.Fail("Found code problems.");
        });
}