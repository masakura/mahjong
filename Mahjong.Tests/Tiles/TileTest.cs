﻿using PowerAssert;

namespace Mahjong.Tiles;

public sealed class TileTest
{
    [Fact]
    public void 麻雀牌文字コードと比較できる()
    {
        PAssert.IsTrue(() => Tile.Characters[0] == "🀇");
    }

    [Fact]
    public void 牌のマーク()
    {
        PAssert.IsTrue(() => Tile.Characters[3].Mark == "🀊");
    }

    [Fact]
    public void 萬子()
    {
        PAssert.IsTrue(() => Tile.Characters == "🀇🀈🀉🀊🀋🀌🀍🀎🀏");
    }

    [Fact]
    public void 筒子()
    {
        PAssert.IsTrue(() => Tile.Circles == "🀙🀚🀛🀜🀝🀞🀟🀠🀡");
    }

    [Fact]
    public void 索子()
    {
        PAssert.IsTrue(() => Tile.Bamboo == "🀐🀑🀒🀓🀔🀕🀖🀗🀘");
    }

    [Fact]
    public void 風牌()
    {
        PAssert.IsTrue(() => Tile.PrevailingWind == "🀀🀁🀂🀃");
    }

    [Fact]
    public void 三元牌()
    {
        PAssert.IsTrue(() => Tile.Dragon == "🀆🀅🀄");
    }

    [Fact]
    public void 牌画像から牌を取得する()
    {
        PAssert.IsTrue(() => Tile.Get("🀗") == "🀗");
    }

    [Fact]
    public void 数牌の種類と番号()
    {
        var target = Tile.Characters[0];

        var actual = new
        {
            target.Type,
            target.Number
        };

        PAssert.IsTrue(() => actual.Equals(new
        {
            Type = TileType.Character,
            Number = (byte)1
        }));
    }

    [Fact]
    public void すべての牌()
    {
        PAssert.IsTrue(() => Tile.All.ToString() ==
                             "🀇🀈🀉🀊🀋🀌🀍🀎🀏" +
                             "🀙🀚🀛🀜🀝🀞🀟🀠🀡" +
                             "🀐🀑🀒🀓🀔🀕🀖🀗🀘" +
                             "🀀🀁🀂🀃" +
                             "🀆🀅🀄"
        );
    }

    [Fact]
    public void TestParse()
    {
        var tiles = Tile.Parse("🀌🀛🀖🀂🀅");

        PAssert.IsTrue(() => tiles == "🀌🀛🀖🀂🀅");
    }

    [Fact]
    public void TestSort()
    {
        var tiles = Tile.Parse("🀖🀎🀅🀛🀌🀂");

        PAssert.IsTrue(() => tiles.Sort() == "🀌🀎🀛🀖🀂🀅");
    }
}