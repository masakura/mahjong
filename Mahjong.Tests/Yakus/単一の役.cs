﻿using Mahjong.Hands;
using PowerAssert;

namespace Mahjong.Yakus;

public sealed class 単一の役
{
    private readonly YakuRules _target = YakuRules.Instance;

    [Fact]
    public void 役なし()
    {
        var handSet = new Hand("🀇🀇🀏🀙🀡🀘🀀🀁🀂🀃🀆🀅🀄")
            .AddDrewTile("🀏");

        var actual = _target.Judge(handSet);
        
        PAssert.IsTrue(() => actual == Array.Empty<Yaku>());
    }
    
    [Fact]
    public void 国士無双()
    {
        var handSet = new Hand("🀇🀇🀏🀙🀡🀘🀀🀁🀂🀃🀆🀅🀄")
            .AddDrewTile("🀐");

        var actual = _target.Judge(handSet);

        PAssert.IsTrue(() => actual == new[]
        {
            Yaku.ThirteenOrphans
        });
    }
}