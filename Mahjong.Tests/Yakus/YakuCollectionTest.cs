﻿using PowerAssert;

namespace Mahjong.Yakus;

public sealed class YakuCollectionTest
{
    [Fact]
    public void 順番が違っても等価()
    {
        var left = new YakuCollection(new[] { Yaku.ThirteenOrphans, Yaku.AllRuns });
        var right = new YakuCollection(new[] { Yaku.AllRuns, Yaku.ThirteenOrphans });
        
        PAssert.IsTrue(() => left == right);
    }
}