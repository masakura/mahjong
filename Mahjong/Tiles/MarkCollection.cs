﻿using System.Collections.Immutable;
using System.Globalization;

namespace Mahjong.Tiles;

public sealed class MarkCollection
{
    private readonly ImmutableArray<string> _marks;

    public MarkCollection(IEnumerable<string> marks)
    {
        _marks = marks.ToImmutableArray();
    }

    public static MarkCollection Parse(string marks)
    {
        return new MarkCollection(GetTextElements(marks).ToArray());
    }

    public override string ToString()
    {
        return string.Join("", _marks);
    }

    private static IEnumerable<string> GetTextElements(string marks)
    {
        var enumerator = StringInfo.GetTextElementEnumerator(marks);

        while (enumerator.MoveNext()) yield return enumerator.GetTextElement();
    }

    public static implicit operator string(MarkCollection marks)
    {
        return marks.ToString();
    }

    internal IEnumerable<Tile> Select(Func<string, Tile> selector)
    {
        return _marks.Select((m, _) => selector(m));
    }

    internal IEnumerable<Tile> Select(Func<string, byte, Tile> selector)
    {
        return _marks.Select((m, i) => selector(m, (byte)(i + 1)));
    }
}