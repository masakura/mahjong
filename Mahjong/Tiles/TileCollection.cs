﻿using System.Collections.Immutable;

namespace Mahjong.Tiles;

public sealed class TileCollection
{
    private readonly ImmutableDictionary<string, Tile> _markMap;
    private readonly ImmutableArray<Tile> _tiles;

    public TileCollection(IEnumerable<Tile> tiles)
    {
        _tiles = tiles.ToImmutableArray();
        _markMap = _tiles.ToImmutableDictionary(
            t => t.Mark,
            t => t
        );
    }

    public Tile this[int i] => _tiles[i];
    public static TileCollection Empty { get; } = new(Enumerable.Empty<Tile>());

    public Tile Get(string mark)
    {
        return _markMap[mark];
    }


    private MarkCollection Marks()
    {
        return new MarkCollection(_tiles.Select(t => t.Mark));
    }

    public override string ToString()
    {
        return Marks().ToString();
    }

    public static implicit operator string(TileCollection tiles)
    {
        return tiles.ToString();
    }

    public static implicit operator TileCollection(string marks)
    {
        return Tile.GetAll(marks);
    }

    #region コレクションを調べる

    /**
     * 引数に与えられた牌をすべて含んでいるかどうかを取得します。
     */
    public bool ContainsAll(TileCollection tiles)
    {
        return tiles._tiles.All(Contains);
    }

    private bool Contains(Tile tile)
    {
        return _tiles.Contains(tile);
    }

    #endregion

    #region コレクション操作

    public TileCollection Concat(TileCollection other)
    {
        return new TileCollection(
            _tiles.Union(other._tiles)
        );
    }

    public TileCollection Add(Tile tile)
    {
        return new TileCollection(
            _tiles.Union(new[] { tile })
        );
    }

    public TileCollection Sort()
    {
        return new TileCollection(_tiles.Sort());
    }

    #endregion
}