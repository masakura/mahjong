﻿namespace Mahjong.Tiles;

public readonly struct TileType : IEquatable<TileType>
{
    private readonly string _label;
    internal MarkCollection Marks { get; }

    private TileType(string label, string marks)
    {
        _label = label;
        Marks = MarkCollection.Parse(marks);
    }

    public static TileType Character { get; } = new("萬子", "🀇🀈🀉🀊🀋🀌🀍🀎🀏");
    public static TileType Circle { get; } = new("筒子", "🀙🀚🀛🀜🀝🀞🀟🀠🀡");

    public static TileType Bamboo { get; } = new("索子", "🀐🀑🀒🀓🀔🀕🀖🀗🀘");
    public static TileType PrevailingWind { get; } = new("風牌", "🀀🀁🀂🀃");
    public static TileType Dragon { get; } = new("三元牌", "🀆🀅🀄");

    public override string ToString()
    {
        return _label;
    }

    public bool Equals(TileType other)
    {
        return _label == other._label;
    }

    public override bool Equals(object? obj)
    {
        return obj is TileType other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _label.GetHashCode();
    }

    public static bool operator ==(TileType left, TileType right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(TileType left, TileType right)
    {
        return !left.Equals(right);
    }
}