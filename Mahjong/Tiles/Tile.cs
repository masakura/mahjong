﻿namespace Mahjong.Tiles;

public readonly struct Tile : IEquatable<Tile>, IComparable<Tile>
{
    private readonly byte _order;

    private Tile(string mark, TileType type, byte number, byte order)
    {
        _order = order;
        Mark = mark;
        Type = type;
        Number = number;
    }

    static Tile()
    {
        var factory = new TilesFactory(Create);
        Characters = factory.Create(TileType.Character);
        Circles = factory.Create(TileType.Circle);
        Bamboo = factory.Create(TileType.Bamboo);
        PrevailingWind = factory.Create(TileType.PrevailingWind);
        Dragon = factory.Create(TileType.Dragon);
        
        
        All =  TileCollection.Empty
            .Concat(Characters)
            .Concat(Circles)
            .Concat(Bamboo)
            .Concat(PrevailingWind)
            .Concat(Dragon);
    }

    public static TileCollection Characters { get; }
    public static TileCollection Circles { get; }
    public static TileCollection Bamboo { get; }
    public static TileCollection PrevailingWind { get; }
    public static TileCollection Dragon { get; }

    public static TileCollection All { get; }

    public TileType Type { get; }
    public byte Number { get; }
    public string Mark { get; }

    public override string ToString()
    {
        return Mark;
    }

    public static Tile Get(string mark)
    {
        return All.Get(mark);
    }
    
    public static TileCollection GetAll(string marks)
    {
        var tiles = MarkCollection.Parse(marks).Select(Get);
        return new TileCollection(tiles);
    }

    public static TileCollection Parse(string marks)
    {
        var tiles = MarkCollection.Parse(marks).Select(Get);
        return new TileCollection(tiles);
    }

    public static implicit operator string(Tile tile)
    {
        return tile.ToString();
    }

    public bool Equals(Tile other)
    {
        return Mark == other.Mark;
    }

    public override bool Equals(object? obj)
    {
        return obj is Tile other && Equals(other);
    }

    public override int GetHashCode()
    {
        return Mark.GetHashCode();
    }

    public static bool operator ==(Tile left, Tile right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(Tile left, Tile right)
    {
        return !left.Equals(right);
    }

    public int CompareTo(Tile other)
    {
        return _order.CompareTo(other._order);
    }

    private static Tile Create(string mark, TileType type, byte number, byte order)
    {
        return new Tile(mark, type, number, order);
    }

    public static implicit operator Tile(string mark)
    {
        return Get(mark);
    }
}