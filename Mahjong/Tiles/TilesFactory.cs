﻿namespace Mahjong.Tiles;

internal sealed class TilesFactory
{
    private readonly Func<string, TileType, byte, byte, Tile> _createTile;
    private byte _order;

    public TilesFactory(Func<string, TileType, byte, byte, Tile> createTile)
    {
        _createTile = createTile;
    }

    public TileCollection Create(TileType type)
    {
        var tiles = type.Marks.Select((mark, number) => _createTile(mark, type, number, _order++));
        return new TileCollection(tiles);
    }
}