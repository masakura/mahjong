﻿using Mahjong.Hands;
using Mahjong.Tiles;

namespace Mahjong.Yakus;

internal class ThirteenOrphansYakuRule : IYakuRule
{
    private static readonly TileCollection Patterns = "🀇🀏🀙🀡🀐🀘🀀🀁🀂🀃🀆🀅🀄";

    public static ThirteenOrphansYakuRule Instance { get; } = new();

    public Yaku? Judge(HandSet handSet)
    {
        if (handSet.All.ContainsAll(Patterns))
            return Yaku.ThirteenOrphans;
        return null;
    }
}