﻿namespace Mahjong.Yakus;

public sealed class Yaku : IEquatable<Yaku>, IComparable<Yaku>
{
    private readonly string _name;

    private Yaku(string name)
    {
        _name = name;
    }

    /// <summary>
    ///     国士無双。
    /// </summary>
    public static Yaku ThirteenOrphans { get; } = new("国士無双");

    /// <summary>
    ///     平和。
    /// </summary>
    public static Yaku AllRuns { get; } = new("平和");

    public bool Equals(Yaku? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return _name == other._name;
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || (obj is Yaku other && Equals(other));
    }

    public override int GetHashCode()
    {
        return _name.GetHashCode();
    }

    public override string ToString()
    {
        return _name;
    }

    public int CompareTo(Yaku? other)
    {
        if (ReferenceEquals(this, other)) return 0;
        if (ReferenceEquals(null, other)) return 1;
        return string.Compare(_name, other._name, StringComparison.Ordinal);
    }
}