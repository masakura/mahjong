﻿using Mahjong.Hands;

namespace Mahjong.Yakus;

public sealed class YakuRules
{
    private readonly IEnumerable<IYakuRule> _rules = new[]
    {
        ThirteenOrphansYakuRule.Instance
    };


    public static YakuRules Instance { get; } = new();

    public YakuCollection Judge(HandSet handSet)
    {
        var yakus = _rules
            .Select(rule => rule.Judge(handSet))
            .OfType<Yaku>();
        return new YakuCollection(yakus);
    }
}