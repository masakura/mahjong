﻿using Mahjong.Hands;

namespace Mahjong.Yakus;

public interface IYakuRule
{
    Yaku? Judge(HandSet handSet);
}