﻿using System.Collections.Immutable;

namespace Mahjong.Yakus;

public sealed class YakuCollection : IEquatable<YakuCollection>
{
    private readonly ImmutableArray<Yaku> _items;

    public YakuCollection(IEnumerable<Yaku> items)
    {
        _items = items.ToImmutableArray().Sort();
    }

    public bool Equals(YakuCollection? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Enumerable.SequenceEqual(_items, other._items);
    }

    public override bool Equals(object? obj)
    {
        return ReferenceEquals(this, obj) || obj is YakuCollection other && Equals(other);
    }

    public override int GetHashCode()
    {
        return _items.GetHashCode();
    }

    public static bool operator ==(YakuCollection? left, YakuCollection? right)
    {
        return Equals(left, right);
    }

    public static bool operator !=(YakuCollection? left, YakuCollection? right)
    {
        return !Equals(left, right);
    }

    public override string ToString()
    {
        return $"[{string.Join(", ", _items.Select(i => i.ToString()))}]";
    }

    public static implicit operator YakuCollection(Yaku[] items)
    {
        return new YakuCollection(items);
    }
}