﻿using Mahjong.Tiles;

namespace Mahjong.Hands;

public sealed class HandSet
{
    public HandSet(Hand hand, Tile tile)
    {
        All = hand.Tiles.Add(tile);
    }

    public TileCollection All { get; }
}