﻿using Mahjong.Tiles;

namespace Mahjong.Hands;

public sealed class Hand
{
    public TileCollection Tiles { get; }

    public Hand(TileCollection tiles)
    {
        Tiles = tiles;
    }

    public HandSet AddDrewTile(Tile tile)
    {
        return new HandSet(this, tile);
    }
}